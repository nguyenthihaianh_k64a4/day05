<?php
session_start();
if (empty($_SESSION['user']) && count($_SESSION['user']) != 6) {
    header("location:index.php");
}
if (!empty($_POST)) {
    session_destroy();
    header("location:index.php");;
}

$gender_mapper = array(0 => "Nam", 1 => "Nữ");
$department_mapper = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");

?>
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">

    <!-- base style  -->
    <link rel="stylesheet" href="styles.css">

    <!-- bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <title>Confirm</title>
</head>

<body>
<div class="confirm">
    <form action="" enctype="multipart/form-data" method="post">
        <div class="input-box">
            <label for="" class="text-label">Họ và tên</label>
            <p><?php echo $_SESSION['user']['name'] ?></p>
        </div>
        <div class="input-box">
            <label for="" class="text-label">Giới tính</label>
            <p><?php echo $gender_mapper[$_SESSION['user']['gender']] ?></p>
        </div>
        <div class="input-box">
            <label for="department" class="text-label">Phân khoa</label>
            <p><?php echo $department_mapper[$_SESSION['user']['department']] ?></p>
        </div>
        <div class="input-box">
            <label for="" class="text-label">Ngày sinh</label>
            <p><?php echo $_SESSION['user']['birthday'] ?></p>
        </div>
        <div class="input-box">
            <label for="" class="text-label"> Địa chỉ </label>
            <p><?php echo $_SESSION['user']['address'] ?></p>
        </div>
        <div class="input-box image-box">
            <label for="" class="text-label"> Hình ảnh </label>
            <?php
            if ($_SESSION['user']['image'] != '') {
                echo '<img src="' . $_SESSION['user']['image'] . '">';
            }
            ?>
        </div>
        <div class="btn">
            <button class="btn-submit" type="submit" name="submit">Xác nhận</button>
        </div>
    </form>
</div>
</body>
</html>
